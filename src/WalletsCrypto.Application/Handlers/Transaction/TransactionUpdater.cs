﻿using System;
using System.Threading.Tasks;
using WalletsCrypto.Application.Services;
using WalletsCrypto.Domain.TransactionModule;
using WalletsCrypto.ReadModel.Persistence;
using TransactionReadModel = WalletsCrypto.ReadModel.Transaction.Transaction;

namespace WalletsCrypto.Application.Handlers.Transaction
{
    public class TransactionUpdater : IDomainEventHandler<TransactionId, TransactionCreatedEvent>
    {
        private readonly IRepository<TransactionReadModel> _transactionRepository;
        public TransactionUpdater(IRepository<TransactionReadModel> transactionRepository)
        {
            _transactionRepository = transactionRepository;
        }

        public async Task HandleAsync(TransactionCreatedEvent @event)
        {
            await _transactionRepository.InsertAsync(new TransactionReadModel
            {
                Id = @event.AggregateId.IdAsStringWithoutPrefix(),
                UserId = @event.UserId.IdAsStringWithoutPrefix(),
                AddressId = @event.AddressId.IdAsStringWithoutPrefix(),
                TransactionAmount = @event.TransactionAmount.Value + 0.00m, // adding this incase there's no decimal place to number
                TransactionAddress = @event.TransactionAddress.AddressString,
                TransactionType = @event.TransactionType.Type,
                TransactionFee = @event.TransactionFee is object ? @event.TransactionFee.Value + 0.00m : 0.00m
            });


        }

    }
}
