﻿using System.ComponentModel.DataAnnotations;

namespace WalletsCrypto.Models.Address
{
    public class UpdateAddressModel
    {
        [Required]
        public decimal Balance { get; set; }
    }
}