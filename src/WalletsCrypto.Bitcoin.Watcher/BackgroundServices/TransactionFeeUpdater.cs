﻿using BitcoinLib.Services.Coins.Base;
using BitcoinLib.Services.Coins.Bitcoin;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using System;
using System.Threading;
using System.Threading.Tasks;
using WalletsCrypto.Common.Configuration;
using WalletsCrypto.Infrastructure.Cache;

namespace WalletsCrypto.Bitcoin.Watcher.BackgroundServices
{
    public class TransactionFeeUpdater : BackgroundService
    {
        private readonly ICacheStorage _cache;
        private readonly ICoinService _coinService;
        private readonly ILogger<TransactionFeeUpdater> _logger;
        public TransactionFeeUpdater(
            ICacheStorage cache,
            ILogger<TransactionFeeUpdater> logger)
        {
            _cache = cache;
            _logger = logger;
            _coinService = new BitcoinService(ApplicationConfiguration.BitcoinNodeConfiguration.RPCUrl,
                ApplicationConfiguration.BitcoinNodeConfiguration.RPCUsername,
                ApplicationConfiguration.BitcoinNodeConfiguration.RPCPassword,
                ApplicationConfiguration.BitcoinNodeConfiguration.WalletPassword,
                ApplicationConfiguration.BitcoinNodeConfiguration.RPCRequestTimeout);
        }
        protected async override Task ExecuteAsync(CancellationToken stoppingToken)
        {
            while(!stoppingToken.IsCancellationRequested)
            {
                try
                {
                    var fee = _coinService.EstimateSmartFee(100);
                    await _cache.StoreAsync("CURRENT_BITCOIN_TRANSACTION_FEE", fee.FeeRate.ToString());
                    await Task.Delay(TimeSpan.FromMinutes(5));
                }
                catch (Exception ex)
                {
                    _logger.LogError(ex.ToString());
                }
                
            }
        }
    }
}
